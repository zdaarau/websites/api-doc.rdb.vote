# [api-doc.rdb.vote](https://api-doc.rdb.vote/)

[![Health](https://status.rdb.vote/api/v1/endpoints/static-sites_api-documentation-(swagger-ui)/health/badge.svg)](https://status.rdb.vote/endpoints/static-sites_api-documentation-(swagger-ui))
[![Uptime (24h)](https://status.rdb.vote/api/v1/endpoints/static-sites_api-documentation-(swagger-ui)/uptimes/24h/badge.svg)](https://status.rdb.vote/endpoints/static-sites_api-documentation-(swagger-ui))
[![Response time (24h)](https://status.rdb.vote/api/v1/endpoints/static-sites_api-documentation-(swagger-ui)/response-times/24h/badge.svg)](https://status.rdb.vote/endpoints/static-sites_api-documentation-(swagger-ui))
[![Netlify Status](https://api.netlify.com/api/v1/badges/4319fffa-706b-45ae-8d8b-1015de89e0ab/deploy-status)](https://app.netlify.com/sites/api-doc-rdb-vote/deploys)

RDB API documentation provided by statically served [Swagger UI](https://swagger.io/tools/swagger-ui/) that generates pages client-side from the [RDB OpenAPI
specification](https://api.rdb.vote/).

## License

Code and configuration in this repository is licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[`LICENSE.md`](LICENSE.md).
