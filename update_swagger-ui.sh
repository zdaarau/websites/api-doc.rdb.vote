#!/bin/bash
#
# Requirements:
#
# - CLI tools:
#   - [Bash](https://en.wikipedia.org/wiki/Bash_(Unix_shell)) (obviously)
#   - [coreutils](https://en.wikipedia.org/wiki/List_of_GNU_Core_Utilities_commands)
#   - [findutils](https://en.wikipedia.org/wiki/Findutils)
#   - [`curl`](https://curl.se/)
#   - [`dasel`](https://daseldocs.tomwright.me/)
#   - [`git`](https://git-scm.com/)
#   - [`sd`](https://github.com/chmln/sd)
#   - [`tar`](https://en.wikipedia.org/wiki/List_of_GNU_packages)

# ensure required CLI tools are available
if ! command -v curl >/dev/null ; then
  echo "\`curl\` is required but not found on PATH. See https://curl.se/download.html"
  exit 1
fi
if ! command -v dasel >/dev/null ; then
  echo "\`dasel\` is required but not found on PATH. See https://daseldocs.tomwright.me/installation"
  exit 1
fi
if ! command -v git >/dev/null ; then
  echo "\`git\` is required but not found on PATH. See https://git-scm.com/downloads"
  exit 1
fi
if ! command -v sd >/dev/null ; then
  echo "\`sd\` is required but not found on PATH. See https://github.com/chmln/sd#readme"
  exit 1
fi
if ! command -v tar >/dev/null ; then
  echo "\`tar\` is required but not found on PATH."
  exit 1
fi

# download latest release metadata
RELEASE_JSON=$(curl --silent https://api.github.com/repos/swagger-api/swagger-ui/releases/latest)
RELEASE_VERSION=$(echo "${RELEASE_JSON}" | dasel --read json 'tag_name' | sd '"' '')

# download and extract latest release
echo "${RELEASE_JSON}" | \
  dasel --read json 'tarball_url' | \
  xargs --max-args=1 curl --silent --location | \
  tar --extract --gzip --file=-

# replace static asset files in /public
rm --recursive public
mkdir --parents public
cp --recursive swagger*/dist/* public/
rm --recursive swagger*

# customize config
sd  'url: ".*?"' 'url: "https://api.rdb.vote/"' public/swagger-initializer.js

# stage, commit and push changes
git add public/
git commit -m "chore: update Swagger UI to ${RELEASE_VERSION}"
git push
